package io.sages.calc.scientific;

import io.sages.calc.Calculator;
import io.sages.calc.CalculatorException;

import java.util.Arrays;

public /*concrete*/ class ScientificCalculator extends Calculator {

    public ScientificCalculator(){
        super();
        System.out.println("[ScientificCalculator] default constructor");
    }

    public ScientificCalculator(double result){
        super(result);
        System.out.println("[ScientificCalculator] parametrized constructor: " + result);
    }

    public double power(int operand){
        memory.setCurrentValue( Math.pow(memory.getCurrentValue(),operand) );
        return memory.getCurrentValue();
    }


    @Override
    public double divide(double operand) throws CalculatorException {
        if(operand==0){
            System.out.println("WARNING: nie dziel przez zero!");
            throw new CalculatorException("nie dziel przez zero!");
        }
        return super.divide(operand);
    }

    @Override
    public void displayResult() {
        System.out.println("[ScientificCalculator] result: " + Arrays.toString(memory.dump()));
    }
}
