package io.sages.calc;

public interface Memory {

    double getCurrentValue();

    void setCurrentValue(double value);

    double[] dump();

}
