package io.sages.calc;

import io.sages.calc.scientific.ScientificCalculator;

public class CalculatorMain {

    public static void main(String[] args) { // main || psvm
        System.out.println("Let's count!"); // sout

        Calculator c1 = new ScientificCalculator(2);
        ScientificCalculator c2 = new ScientificCalculator();

        c1.add(2);
        c1.add(2);
        c1.add(2);
        c1.add(2);
        c1.multiply(3);
        //c1.divide(6);
        System.out.println("c1.result=" + c1.getResult());
        c1.displayResult();

        c2.toString();
        c2.add(2);
        c2.power(8);
        try {
            c2.divide(0);
        } catch (CalculatorException e){
            e.printStackTrace();
            //System.exit(102);
        } finally {
            System.out.println("in finally block.");
        }
        System.out.println("c2.result=" + c2.getResult());


        System.out.println("done.");
    }





}
