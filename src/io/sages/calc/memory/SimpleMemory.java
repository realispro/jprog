package io.sages.calc.memory;

import io.sages.calc.Memory;

public class SimpleMemory implements Memory {

    private double value;

    @Override
    public double getCurrentValue() {
        return value;
    }

    @Override
    public void setCurrentValue(double value) {
        this.value = value;
    }

    @Override
    public double[] dump() {
        return new double[]{value};
    }
}
