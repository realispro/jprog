package io.sages.calc.memory;

import io.sages.calc.Memory;

import java.util.ArrayList;
import java.util.List;

public class ListMemory implements Memory {

    private List<Double> values = new ArrayList<>();

    @Override
    public double getCurrentValue() {
        return values.get(values.size()-1);
    }

    @Override
    public void setCurrentValue(double value) {
        values.add(value);
    }

    @Override
    public double[] dump() {

        double[] array = new double[values.size()];
        for(int i=0; i<values.size(); i++){
            array[i] = values.get(i);
        }
        return array;
    }
}
