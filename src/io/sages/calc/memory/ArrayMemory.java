package io.sages.calc.memory;

import io.sages.calc.Memory;

public class ArrayMemory implements Memory {

    // [0, 0, 0, 0, 0]
    private double[] values;

    private int index = -1;

    public ArrayMemory(int size){
        values = new double[size];
    }

    public ArrayMemory(){
        this(5);
    }

    @Override
    public double getCurrentValue() {
        return values[index];
    }

    @Override
    public void setCurrentValue(double value) {
        // v1: [1,2,3,4,5] + 6 -> [2,3,4,5,6] index:4
        if(index==values.length-1) {
            System.arraycopy(values, 1, values, 0, values.length - 1);
        } else {
            index++;
        }

        // v2: [1,2,3,4,5] + 6 -> [6,2,3,4,5] index:0
        /*;
        index = index%values.length;*/

        values[index] = value;
    }

    @Override
    public double[] dump() {
        double[] dump = new double[index+1];
        // [1,2,0,0,0] -> [1,2]
        System.arraycopy(values, 0, dump, 0, index+1);
        return dump;
    }
}
