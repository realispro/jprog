package io.sages.calc;

import io.sages.calc.memory.ListMemory;

public abstract class Calculator extends Object{

    protected Memory memory = new ListMemory();

    public Calculator(double result){
        super();
        memory.setCurrentValue(result);
        System.out.println("[Calculator] parametrized constructor: " + result);
    }

    public Calculator(){
        this(0.0);
        System.out.println("[Calculator] default constructor");
    }

    public double add(double operand){
        memory.setCurrentValue(memory.getCurrentValue()+operand );
        return memory.getCurrentValue();
    }

    public double subtract(double operand){
        memory.setCurrentValue(memory.getCurrentValue()-operand );
        return memory.getCurrentValue();
    }

    public double multiply(double operand){
        memory.setCurrentValue(memory.getCurrentValue()*operand );
        return memory.getCurrentValue();
    }

    public double divide(double operand) throws CalculatorException {
        memory.setCurrentValue(memory.getCurrentValue()/operand );
        return memory.getCurrentValue();
    }

    // JavaBeans: getter + setter or accessor + mutator
    public double getResult() {
        return memory.getCurrentValue();
    }

    public abstract void displayResult();


    public static int multiply(int... operands){ // var-args

        int result = 1;

        for(int operand : operands){
            result *= operand;
        }

        return result;
    }
}
