package io.sages;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeMain {

    public static void main(String[] args) {

        WeekDay weekDay = WeekDay.FRIDAY;
        System.out.println( " weekDay = " + weekDay.isWeekend());

        // **** legacy API

        Date date = new Date();
        long millis = date.getTime();
        millis++;
        date.setTime(millis);

        Calendar calendar = Calendar.getInstance();
        //calendar.setTime(date);
        calendar.set(2023, Calendar.DECEMBER, 14, 9, 50, 59);
        //calendar.add(Calendar.HOUR,1000);
        date = calendar.getTime();

        Locale locale = new Locale("pl", "PL");
                //Locale.JAPAN;
                //Locale.getDefault();
        DateFormat format = new SimpleDateFormat("E w D MM/dd/yyyy hh:mm:ss:SS a X", locale);
                //DateFormat.getTimeInstance(DateFormat.FULL);
                //DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, locale);
        String timestamp = format.format(date);
        System.out.println("timestamp = " + timestamp);


        // **** java.time

        LocalTime lt = LocalTime.now();
        LocalDate ld = LocalDate.now();
        LocalDateTime ldt = LocalDateTime.of(ld, lt);//.plusDays(10).plusYears(1);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E w D MM/dd/yyyy hh:mm:ss:SS a");

        timestamp = formatter.format(ldt);
        System.out.println("[java.time] timestamp = " + timestamp);

        LocalDate christmasEve = LocalDate.of(2022, 12, 24);

        Period period = Period.between(ld, christmasEve);
        System.out.println("period = " + period);

        ZonedDateTime warsawNow = ldt.atZone(ZoneId.of("Europe/Warsaw"));
                //.now()

        ZonedDateTime warsawLanding = warsawNow.plusHours(7);
        ZonedDateTime singaporeLanding = warsawLanding.withZoneSameInstant(ZoneId.of("Asia/Singapore"));
        System.out.println("singaporeLanding = " + singaporeLanding);

        // TODO 1: remaining days till New Years Eve
        LocalDate newYearsEve = LocalDate.of(ld.getYear(), Month.DECEMBER, 31);

        Period periodToNewYearEve = Period.between(ld, newYearsEve);
        System.out.println("to do 1 = " + periodToNewYearEve);

        // TODO 2: New Years Eve day of a week
        System.out.println("TODO2: New Year's Eve day of the week: " + newYearsEve.getDayOfWeek());

        // TODO 3: is current year leap?
        int year = 2022;
        System.out.println("isLeapYearNow? : " + Year.of(year).isLeap());
        boolean leap = year % 4 == 0 && year % 100 != 0;

        // -ea VM option required
        assert leap==Year.of(year).isLeap() : "incorrect leap year calculation";


        if (leap){
            System.out.println("is leap");
        } else {
            System.out.println("is not leap");
        }

        // TODO 4: if Christmas Eve happen already this year
        if(ld.isBefore(christmasEve)){
            System.out.println("to do 4: Christmas Eve dopiero przed nami");
        }
        else {
            System.out.println("to do 4: Christmas Eve juz za nami");
        }


    }


}
