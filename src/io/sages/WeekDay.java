package io.sages;

public enum WeekDay {

    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY(false),
    FRIDAY(true),
    SATURDAY(true),
    SUNDAY(true);

    private boolean weekend;

    WeekDay(boolean weekend){
        this.weekend = weekend;
    }

    WeekDay(){
        this(false);
    }

    public boolean isWeekend() {
        return weekend;
    }
}
