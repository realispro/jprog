package io.sages;

import io.sages.calc.Calculator;

import java.util.Arrays;

public class HelloWorld {

    public static void main(String[] args) {
        System.out.println("Let's start!");

        // ***** primitive data types ********

        Byte weekDay = Byte.valueOf("127");
        weekDay = (byte) (weekDay + 1);

        System.out.println("weekDay = " + weekDay);

        float price = 1_000_000.45F;
        System.out.println("price = " + price);

        boolean weekend = false;
        System.out.println("weekend = " + weekend);

        char c = 65;
        System.out.println("c = " + (short) c);

        // **** arrays ****

        int[] values = /*new int[]*/{1, 2, 3, 4, 5};
        //new int[5];
        /*values[0] = 1;
        values[1] = 2;
        values[2] = 3;
        values[3] = 4;
        values[4] = 5;*/



        System.out.println("values = " + Arrays.toString(values));

        for (int i = 0; i < values.length; i++) {
            System.out.println("values[" + i + "] = " + values[i]);
        }

        for (int value : values) {
            System.out.println("value = " + value);
        }


        int size = args.length > 0 ? Integer.parseInt(args[0]) : 15;

        /*int size = 15;
        if(args.length>0) {
            try {
                size = Integer.parseInt(args[0]);
            }catch (NumberFormatException e){
                e.printStackTrace();
            }
        }*/

        var results = new int[size][size];

        OUTER:
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                if (y == 12) { // skip multiplying by 13
                    continue;
                }

                results[x][y] = Calculator.multiply(x+1, y+1);
            }
        }


        for (var row : results) {

            for (var value : row) {
                System.out.print(value + "\t");
            }
            System.out.println();

        }


        int z = 11;

        final int rudy = 102;


        switch (z) {
            default:
                System.out.println("default case: " );
                break;
            case 303:
                System.out.println("Dywizjon 303");
                break;
            case rudy:
                System.out.println("Rudy");
                break;
        }


        String season = "spring";

        // java 17
        String temperature = switch (season){
            case "winter", "autumn" -> "cold";
            case "summer", "spring" -> "warm";
            default -> "not sure";
        };

        System.out.println("temperature = " + temperature);

        System.out.println("done.");

        foo(z);
        System.err.println("z = " + z);

        StringBuilder builder = new StringBuilder("foo");
        foo(builder);
        System.out.println("builder = " + builder);


        /*Scanner scanner = new Scanner(System.in);

        while(scanner.hasNext()){
            String next = scanner.next();
            System.out.println("scanner:" + next);
            if(next.equals("exit")){
                break;
            }
        }*/


        double operand1 = 0;
        int operand2 = 0;

        double result = (operand1 / operand2);
        System.out.println("result = " + result);
        System.out.println("int max: " + Integer.MAX_VALUE);

        System.exit(rudy);

    }


    public static void foo(int i){
        i = i + 1;
    }

    public static void foo(StringBuilder builder){
        builder.append("bar");
    }


/*    public static int multiply(int x, int y){
        return x * y;
    }*/

}
