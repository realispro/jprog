package io.sages.travel;

public class Bus extends Vehicle implements Transportation{
    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is being transported by bus");
    }

    @Override
    public int getSpeed() {
        return 90;
    }
}
