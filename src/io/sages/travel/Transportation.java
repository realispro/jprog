package io.sages.travel;

@FunctionalInterface
public interface Transportation {

    void transport(String passenger);

    // Java 8
    static String foo(){
        return "bar";
    }

    // Java 8
    default int getSpeed(){
        return -1 * factor();
    }

    // Java 9
    private int factor(){
        return 2;
    }

}
