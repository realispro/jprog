package io.sages.travel;

public class TravelMain {

    public static void main(String[] args) {
        System.out.println("Let's travel! " + Transportation.foo());

        String traveller = "Jan Kowalski";

        Transportation t = getTransportation();
        System.out.println("transportation speed: " + t.getSpeed());
        t.transport(traveller);

    }


    public static Transportation getTransportation(){
        return p -> System.out.println("teleporting passenger " + p);
                //new Bus();
    }

}
