package io.sages.dev;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public final class TeamMember {

    private final String name;
    private final Set<Skill> skills = new HashSet<>();
    private final Set<Role> roles = new HashSet<>();

    public TeamMember(String name) {
        this.name = name;
    }

    public TeamMember withSkill(Skill skill){
        skills.add(skill);
        return this;
    }

    public TeamMember withRole(Role role){
        roles.add(role);
        return this;
    }

    public void removeRole(Role role){
        roles.remove(role);
    }

    public String getName() {
        return name;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamMember that = (TeamMember) o;
        return Objects.equals(name, that.name) && Objects.equals(skills, that.skills) && Objects.equals(roles, that.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, skills, roles);
    }

    @Override
    public String toString() {
        return "TeamMember{" +
                "name='" + name + '\'' +
                ", skills=" + skills +
                ", roles=" + roles +
                '}';
    }
}
