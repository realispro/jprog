package io.sages.dev;

import java.util.*;
import java.util.stream.Collectors;

public class DevelopmentMain {

    public static void main(String[] args) {

        System.out.println("Let's develop!");
        Team team = constructTeam("A Team");
        List<TeamMember> members = team.getMembers();

        // TODO 1: sort by skills count
        //Comparator<TeamMember> comparator = (m1, m2) -> m1.getSkills().size() - m2.getSkills().size();
        //Collections.sort(members, comparator);

        List<TeamMember> sortedMembers = members.stream()
                .sorted(Comparator.comparingInt(m -> m.getSkills().size()))
                .collect(Collectors.toList());
        System.out.println("sortedMembers = " + sortedMembers);

        System.out.println("end TODO #1");

        // TODO 2: remove technical writer
        System.out.println("TODO #2");

        //Predicate<TeamMember> predicate = m -> m.getRoles().contains(Role.TECHNICAL_WRITER);
        //members.removeIf(predicate);
        /*Iterator<TeamMember> iterator = members.iterator();
        while (iterator.hasNext()) {
            TeamMember currentMember = iterator.next();
            if (currentMember.getRoles().contains(Role.TECHNICAL_WRITER)) {
                iterator.remove();
            }
        }*/
        var res2 = members.stream()
                .filter(m -> !m.getRoles().contains(Role.TECHNICAL_WRITER))
                .toList();

        System.out.println("res 2 = " + res2);


        // TODO 3: find all java developers
        // v1:  new list with java devs
        List<TeamMember> javaDevs = new LinkedList<>();
        members.forEach(m -> {
                    if (m.getSkills().contains(Skill.JAVA)) {
                        javaDevs.add(m);
                    }
                }
        );

        List<TeamMember> javaDevs2  =  members.stream().filter( a -> a.getRoles().contains(Role.DEVELOPER) && a.getSkills().contains(Skill.JAVA)).toList();
        System.out.println("javaDevs2 = " + javaDevs2);


        System.out.println("javaDevs: " + javaDevs);

        // v2: filter non-java devs from member list
        //members.removeIf( m -> !m.getSkills().contains(Skill.JAVA) );

        // v3: using iterator
        /*List<TeamMember> devMembers = new ArrayList<>();
        Iterator<TeamMember> iterator = members.iterator();
        while (iterator.hasNext()) {
            TeamMember currentMember = iterator.next();
            if (currentMember.getRoles().contains(Role.DEVELOPER) && currentMember.getSkills().contains(Skill.JAVA)) {
                devMembers.add(currentMember);
            }
        }

        for(var currentMember : members){
            if (currentMember.getRoles().contains(Role.DEVELOPER) && currentMember.getSkills().contains(Skill.JAVA)) {
                devMembers.add(currentMember);
            }
        }*/

        // TODO 4: count all javascript developers

        // v1: filtering out non js and no devs
        var javascriptDevs = new ArrayList<>(members);
        /*javascriptDevs.removeIf(m -> !m.getSkills().contains(Skill.JAVASCRIPT));
        javascriptDevs.removeIf((m ->!m.getRoles().contains(Role.DEVELOPER)));
        System.out.println("number of JAVASCRIPT developers=" + javascriptDevs.size());*/

        // v2:
        long counter=0;
        Iterator<TeamMember> iterator = members.iterator();
        while (iterator.hasNext()) {
            TeamMember currentMember = iterator.next();
            if(currentMember.getRoles().contains(Role.DEVELOPER) && currentMember.getSkills().contains(Skill.JAVASCRIPT)) {
                System.out.println("Znalazlem JAVASCRIPT developera, jest nim: " + currentMember.getName());
                counter++;
            }
        }
        counter = members.stream()
                .filter(m-> m.getSkills().contains(Skill.JAVASCRIPT) && m.getRoles().contains(Role.DEVELOPER))
                .count();


        System.out.println("counter = " + counter);

        members.forEach(m -> System.out.println("member = " + m));
        System.out.println("done.");

    }

    public static Team constructTeam(String name) {
        Team team = new Team(name);
        TeamMember joe = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);

        TeamMember joeBis = new TeamMember("Joe")
                .withSkill(Skill.JAVA)
                .withSkill(Skill.JPA)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER);
        team.addMember(joeBis);
        team.addMember(new TeamMember("Jane")
                .withSkill(Skill.ANGULAR)
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.JAVA)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SPRING)
                .withRole(Role.DEVELOPER)
                .withRole(Role.SCRUM_MASTER));
        team.addMember(new TeamMember("Bob")
                .withSkill(Skill.JAVASCRIPT)
                .withSkill(Skill.REACT_JS)
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withSkill(Skill.SPRING)
                .withRole(Role.QA));
        team.addMember(new TeamMember("Betty")
                .withSkill(Skill.PYTHON)
                .withSkill(Skill.SQL)
                .withRole(Role.QA));

        team.addMember(joe);

        team.addMember(new TeamMember("Billy")
                .withSkill(Skill.SQL)
                .withRole(Role.TECHNICAL_WRITER)
                .withRole(Role.PRODUCT_OWNER));

        return team;
    }
}
