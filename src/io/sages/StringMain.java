package io.sages;

import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringMain {

    public static void main(String[] args) {
        System.out.println("Let's process text!");

        // **** String pool
        String s1 = "Mundial 2022";
        String s2 = "Mundial 2022";

        System.out.println("s1==s2 ? " + (s1==s2));
        System.out.println("s1.equals(s2) ? " + s1.equals(s2));

        // **** tokenization
        String names = "Kowal,Kowalski,Kowalewski";
        String[] namesArray = names.split(",");
        System.out.println("namesArray = " + Arrays.toString(namesArray));
        
        int index = names.indexOf("Kowalski");
        System.out.println("index = " + index);

        boolean found = false;
        StringTokenizer tokenizer = new StringTokenizer(names, ",");
        while(tokenizer.hasMoreElements()){
            String name = tokenizer.nextToken();
            if("Kowalski".equals(name)){
                found = true;
                break;
            }
        }
        System.out.println("found = " + found);
        
        // **** string builder
        //String result = "";
        StringBuilder builder = new StringBuilder();
        for( String name : namesArray){
            builder.append(name).append(",");
            //result = result + name + ",";
        }
        System.out.println("result = " + builder.toString());
        
        // ***** RegExp

        String text = "Warszawa 00-950, Kraków 30512, Toruń 88 100";
        String patternText = "\\d{2}[-\\s]?\\d{3}";

        Pattern pattern = Pattern.compile(patternText);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()){
            System.out.println("found at " + matcher.start() + " group " + matcher.group());
        }


    }
}
