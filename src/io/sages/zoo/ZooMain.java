package io.sages.zoo;

import io.sages.zoo.animals.Eagle;
import io.sages.zoo.animals.Kiwi;
import io.sages.zoo.animals.Shark;
import io.sages.zoo.animals.Tuna;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ZooMain {

    public static void main(String[] args) {
        System.out.println("Let's visit zoo!");

        Eagle bielik = new Eagle("Bielik", 15);
        Eagle george = new Eagle("George", 13);
        Shark nemo = new Shark("Nemo", 199);
        Shark willy = new Shark("Willy", 299);
        Kiwi joe = new Kiwi("Joe", 5);
        Tuna jack = new Tuna("Jack", 50);

        Eagle bielikBis = new Eagle("Bielik", 15);

        List<Animal> animals = new ArrayList<>(List.of(bielik, george, nemo, willy, joe, jack, bielikBis, willy));


        Predicate<Animal> predicate = a -> a.getName().equals("Nemo");
                /*new Predicate<Animal>() {
            @Override
            public boolean test(Animal a) {
                return a.getName().equals("Nemo");
            }
        };*/
        animals.removeIf(a -> a.getName().equals("Nemo"));
        /*Iterator<Animal> itr = animals.iterator();
        while (itr.hasNext()){
            Animal a = itr.next();
            if(a.getName().equals("Nemo")){
                itr.remove();
            }
        }*/

        System.out.println("animals.size()=" + animals.size());

        Comparator<Animal> comparator = (a1, a2) -> a1.getName().compareTo(a2.getName());

                /*new Comparator<Animal>() { // anonymous class
            @Override
            public int compare(Animal a1, Animal a2) {
                return a1.getName().compareTo(a2.getName());
            }
        };*/
                //new AnimalComparator(); // named class

        Collections.sort(animals, (a1, a2) -> a1.getName().compareTo(a2.getName()));

        Consumer<Animal> consumer = a -> System.out.println("animal = " + a);
                /*new Consumer<Animal>() {
            @Override
            public void accept(Animal animal) {
                System.out.println("animal = " + animal);
            }
        };*/
        animals.forEach(a -> System.out.println("animal = " + a));
        /*for(var animal : animals){
            System.out.println("animal = " + animal);
        }*/






    }
}
