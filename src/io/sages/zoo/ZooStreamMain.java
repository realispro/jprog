package io.sages.zoo;

import io.sages.zoo.animals.Eagle;
import io.sages.zoo.animals.Kiwi;
import io.sages.zoo.animals.Shark;
import io.sages.zoo.animals.Tuna;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ZooStreamMain {

    public static void main(String[] args) {

        Eagle bielik = new Eagle("Bielik", 15);
        Eagle george = new Eagle("George", 13);
        Shark nemo = new Shark("Nemo", 199);
        Shark willy = new Shark("Willy", 299);
        Kiwi joe = new Kiwi("Joe", 5);
        Tuna jack = new Tuna("Jack", 50);

        Eagle bielikBis = new Eagle("Bielik", 15);

        List<Animal> animals = new ArrayList<>(List.of(bielik, george, nemo, willy, joe, jack, bielikBis, willy));


        LocalTime start = LocalTime.now();
        Stream<Animal> stream = animals.stream();

        var result =
            stream
                    // ****** intermediate ops
                    .peek(a-> System.out.println("animal[before filter]="+a))
                    .filter( a -> !a.getName().equals("Nemo"))
                    .peek(a-> System.out.println("animal[after filter]="+a))
                    .distinct()
                    .sorted((a1,a2)->a1.getMass()-a2.getMass())
                    .map( a -> a.getName())
                    .peek(n-> System.out.println("name[after map]="+n))

                    // ****** terminal ops
                            //.count();
                            .toList();   //collect(Collectors.toList());
                            //.anyMatch(a->a.getMass()>200);
                                    //.findFirst();
        LocalTime end = LocalTime.now();

        Duration duration = Duration.between(start, end);
        System.out.println("duration = " + duration);


        Supplier<String> supplier = () -> "unknown";
        System.out.println("result = " + result);


        //Stream<Integer> integerStream =

        IntStream integerStream =
                IntStream.iterate(1, i -> i+1);
                //.generate(()->13);

        integerStream
                .limit(13)
                //.sorted()
                .forEach(i-> System.out.println("i=" + i));





    }


    public static Stream<Animal> intermediateOps(Stream<Animal> stream){
        Stream<Animal> stream2 = stream
                .filter( a -> !a.getName().equals("Nemo"))
                .distinct();

        return stream2;
    }
}
