package io.sages.zoo;

import java.util.Objects;

public abstract class Animal implements Comparable<Animal>{

    private String name;
    private int mass;

    public Animal(String name, int mass) {
        this.name = name;
        this.mass = mass;
    }

    public void eat(String food){
        System.out.println(name + " is eating " + food);
    }

    public abstract void move();


    public String getName() {
        return name;
    }

    public int getMass() {
        return mass;
    }

    @Override
    public int compareTo(Animal a) {
        return this.mass - a.mass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return getMass() == animal.getMass() && getName().equals(animal.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getMass());
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", mass=" + mass +
                '}';
    }
}
