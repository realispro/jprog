package io.sages.zoo.animals;

import io.sages.zoo.Bird;

public class Eagle extends Bird {
    public Eagle(String name, int mass) {
        super(name, mass);
    }
}
