package io.sages.zoo.animals;

import io.sages.zoo.Fish;

public class Tuna extends Fish {
    public Tuna(String name, int mass) {
        super(name, mass);
    }
}
